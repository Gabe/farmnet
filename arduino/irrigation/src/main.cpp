      // Feather9x_TX
// -*- mode: C++ -*-
// Example sketch showing how to create a simple messaging client (transmitter)
// with the RH_RF95 class. RH_RF95 class does not provide for addressing or
// reliability, so you should only use RH_RF95 if you do not need the higher
// level messaging abilities.
// It is designed to work with the other example Feather9x_RX

#include <SPI.h>
#include <RH_RF95.h>

// For dragino shield:
#define RFM95_CS 10
#define RFM95_RST 9
#define RFM95_INT 2
// For feather:
// #define RFM95_CS 8
// #define RFM95_RST 4
// #define RFM95_INT 3

#define BOARD_ADDR 2
#define MASTER_ADDR 1


// Change to 434.0 or other frequency, must match RX's freq!
#define RF95_FREQ 868.1

#define NUM_CHANNELS 4
uint8_t message[100];
uint8_t CHANNELS[] = {A0, A1, A2, A3};

// Singleton instance of the radio driver
RH_RF95 rf95(RFM95_CS, RFM95_INT);

void setup() 
{
  pinMode(RFM95_RST, OUTPUT);
  digitalWrite(RFM95_RST, HIGH);

  Serial.begin(9600);
  delay(100);

  Serial.println("Feather LoRa TX Test!");

  // manual reset
  digitalWrite(RFM95_RST, LOW);
  delay(10);
  digitalWrite(RFM95_RST, HIGH);
  delay(10);

  while (!rf95.init()) {
    Serial.println("LoRa radio init failed");
    while (1);
  }
  Serial.println("LoRa radio init OK!");

  // Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM
  if (!rf95.setFrequency(RF95_FREQ)) {
    Serial.println("setFrequency failed");
    while (1);
  }
  Serial.print("Set Freq to: "); Serial.println(RF95_FREQ);
  
  // Defaults after init are 868.1MHz, 13dBm, Bw = 125 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on

  // The default transmitter power is 13dBm, using PA_BOOST.
  // If you are using RFM95/96/97/98 modules which uses the PA_BOOST transmitter pin, then 
  // you can set transmitter powers from 5 to 23 dBm:
  rf95.setTxPower(23, false);

  rf95.setThisAddress(BOARD_ADDR);
  rf95.setHeaderTo(MASTER_ADDR);
  rf95.setHeaderFrom(BOARD_ADDR);

  //Set up pinouts for relays

  for(int i=0; i<NUM_CHANNELS; i++) {
    // This sets the relay to open:
    digitalWrite(CHANNELS[i], HIGH);
    pinMode(CHANNELS[i], OUTPUT);
  }

}

char* subStr (char* str, char *delim, int index) {
  char *act, *sub, *ptr;
  static char copy[RH_RF95_MAX_MESSAGE_LEN];
  int i;
  // Since strtok consumes the first arg, make a copy
  strcpy(copy, str);

  for (i = 1, act = copy; i <= index; i++, act = NULL) {
     sub = strtok_r(act, delim, &ptr);
     if (sub == NULL) break;
  }
  return sub;
}

void parseCommand(char * buf) {
  int idx=0;
  for (int i=0; i<NUM_CHANNELS; i++){
    // We look at the 2nd, 4th, etc. element to get only the command.
    idx = (1+i)*2;
    if (strcmp(subStr(buf, "=;", idx), "1")==0) {
      digitalWrite(CHANNELS[i], LOW);
      Serial.print(i);
      Serial.println("ON");
    } else {
      digitalWrite(CHANNELS[i], HIGH);
      Serial.print(i);
      Serial.println("OFF");
    }
  }
}

void loop()
{
  uint8_t buf[RH_RF95_MAX_MESSAGE_LEN] = {0};
  uint8_t len = sizeof(buf);
  if (rf95.available())
  {
    if (rf95.recv(buf, &len))
    {
     RH_RF95::printBuffer("request: ", buf, len);
     Serial.println("======");
     Serial.print("got request: ");
     Serial.println((char*)buf);
     Serial.print("RSSI: ");
     Serial.println(rf95.lastRssi(), DEC);
     Serial.println("======");
    parseCommand(buf);
    }
  }
  delay(1000);
}
